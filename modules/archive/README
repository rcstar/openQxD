
********************************************************************************

                   I/O functions for field configurations

********************************************************************************


Files
-----

archive.c        Programs to read and write gauge-field configurations.

sarchive.c       Programs to read and write global double-precision spinor
                 fields.

Include file
------------

The file archive.h defines the prototypes for all externally accessible
functions that are defined in the *.c files listed above.


List of functions
-----------------

void write_cnfg(char *out)
  Writes the lattice sizes, the process grid sizes, the coordinates of the
  calling process, the state of the random number generators, the local
  plaquette sums and the local double-precision (SU(3)xU(1) or U(1)/SU(3)
  only) gauge field to the file "out".

int read_cnfg(char *in)
  Detects the type of gauge configuration written in the file "in"
  (i.e. (SU(3)xU(1) or U(1)/SU(3) only). It reads only the gauge fields
  that are active, as returned by the gauge() program defined in
  'modules/flags/lat_parms.c'. The program then resets the random number
  generator and checks that the restored field is compatible with the chosen
  boundary conditions (see the notes). It returns 1 if "in" contains only
  the SU(3) field, 2 if "in" contains only the U(1) field, and 3 if "in"
  contains both. The program assumes that the file "in" was written by the
  program write_cnfg().

void export_cnfg(char *out)
  Writes the lattice sizes and the global double-precision U(1) and/or SU(3)
  gauge field to the file "out" from process 0 in the universal format
  specified below (see the notes).

int import_cnfg(char *in)
  Detects the type of gauge configuration written in the file "in"
  (i.e. (SU(3)xU(1) or U(1)/SU(3) only). It reads only the gauge fields
  that are active, as returned by the gauge() program defined in
  'modules/flags/lat_parms.c'. The program then resets the random number
  generator and checks that the restored field is compatible with the chosen
  boundary conditions. The U(1) and SU(3) fields are periodically extended
  if needed (see the notes). It returns 1 if "in" contains only the SU(3)
  field, 2 if "in" contains only the U(1) field, and 3 if "in" contains
  both. The program assumes that the file "in" was written by the program
  export_cnfg() in the universal format, and file "in" is accessed by
  process 0 only.

void write_sfld(char *out,spinor_dble *sd)
  Writes the lattice sizes, the process grid sizes, the coordinates
  of the calling process, the square of the norm of the spinor field
  sd and the local part of the latter to the file "out".

void read_sfld(char *in,spinor_dble *sd)
  Reads the local part of the spinor field sd from the file "in",
  assuming the field was written to the file by write_sfld().

void export_sfld(char *out,spinor_dble *sd)
  Writes the lattice sizes and the spinor field sd to the file "out"
  from process 0 in the universal format specified below (see the
  notes).

void import_sfld(char *in,spinor_dble *sd)
  Reads the spinor field sd from the file "in" on process 0, assuming
  the field was written to the file in the universal format (see the
  notes).
