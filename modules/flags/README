

********************************************************************************

                       Flags and parameter data base

********************************************************************************

The modules in this directory serve to control the program flow and help
to ensure that the different programs do not interfere with each other
in unintended ways.


Files
-----

action_parms.c   Action parameter data base

dfl_parms.c      Deflation parameters

dirac_parms.c    Dirac-operator parameters

flags.c          Flags data base input and query programs (see README.flags)

force_parms.c    Force parameter data base

hmc_parms.c      Basic HMC parameters

lat_parms.c      Lattice parameters

mdint_parms.c    Molecular-dynamics integrator data base

rat_parms.c      Rational function parameter data base

rw_parms.c       Reweighting factor parameter data base

sap_parms.c      SAP parameters

sf_parms.c       Schroedinger functional boundary values

solver_parms.c   Solver parameter data base

dft_parms.c      DFT parameter data base

dft4d_parms.c    DFT4D parameter data base


Include file
------------

The file flags.h defines the prototypes for all externally accessible
functions that are defined in the *.c files listed above


List of functions
-----------------

action_parms_t set_action_parms(int iact,action_t action,int ipf,
                                int im0,int *irat,int *imu,int *isp)
  Sets the parameters in the action parameter set number iact and returns
  a structure containing them (see the notes).

action_parms_t action_parms(int iact)
  Returns a structure containing the action parameter set number iact
  (see the notes).

void read_action_parms(int iact)
  On process 0, this program scans stdin for a line starting with the
  string "[Action <int>]" (after any number of blanks), where <int> is
  the integer value passed by the argument. An error occurs if no such
  line or more than one is found. The lines

    action   <action_t>
    ipf      <int>
    im0      <int>
    irat     <int> <int> <int>
    imu      <int> [<int>]
    isp      <int> [<int>]

  are then read using read_line() [utils/mutils.c]. Depending on the
  value of "action", some lines are not read and can be omitted in
  the input file. The number of integer items on the lines with tag
  "imu" and "isp" depends on the action too. The data are then added
  to the data base by calling set_action_parms(iact,...).

void print_action_parms(void)
  Prints the parameters of the defined actions to stdout on MPI
  process 0.

void write_action_parms(FILE *fdat)
  Writes the parameters of the defined actions to the file fdat on
  MPI process 0.

void check_action_parms(FILE *fdat)
  Compares the parameters of the defined actions with those stored
  on the file fdat on MPI process 0, assuming the latter were written
  to the file by the program write_action_parms().

dirac_parms_t set_dirac_parms9(int qhat,double m0,
                               double su3csw,double u1csw,
                               double cF,double cF_prime,
                               double th1,double th2,double th3)
  Sets the parameters of the Dirac operator. The adjustable parameters are

    m0             Base Wilson mass.

    qhat           Integer electric charge (in units of the elementary one).

    su3csw         Coefficient of the SU(3) Sheikholeslami-Wohlert term.

    u1csw          Coefficient of the U(1) Sheikholeslami-Wohlert term.

    cF,cF_prime    Fermion action improvement coefficients at time 0 and T,
                   respectively.

    th1,th2,th3    Angles specifying the phase-periodic boundary
                   conditions for the quark fields in direction 1,2,3.

  The return value is a structure that contains all above parameters.

dirac_parms_t dirac_parms_t set_dirac_parms1(dirac_parms_t *par)
  Sets the parameters of the Dirac operator, copying them from the structure
  (*par).

dirac_parms_t dirac_parms(void)
  Returns the parameters currently set for the Dirac operator.

void print_dirac_parms(void);
  Prints the parameters of the Dirac operator to stdout on MPI process 0.

tm_parms_t set_tm_parms(int eoflg)
  Sets the twisted-mass flag. The parameter is

    eoflg          If the flag is set (eoflg!=0), the twisted-mass term
                   in the Dirac operator, the SAP preconditioner and the
                   little Dirac operator is turned off on the odd lattice
                   sites.

  The return value is a structure that contains the twisted-mass flag.

tm_parms_t tm_parms(void)
  Returns a structure containing the twisted-mass flag.

dfl_parms_t set_dfl_parms(int *bs,int Ns)
  Sets the parameters of the deflation subspace. The parameters are

    bs[4]          Sizes of the blocks in DFL_BLOCKS block grid.

    Ns             Number of deflation modes per block (must be
                   even and non-zero).

  The return value is a structure that contains the above parameters.
  Note that these parameters can only be set once.

dfl_parms_t dfl_parms(void)
  Returns the parameters currently set for the deflation subspace.

dfl_pro_parms_t set_dfl_pro_parms(int nkv,int nmx,double res)
  Sets the parameters used when applying the deflation projection in the
  deflated solver program dfl_sap_gcr(). The parameters are

    nkv            Maximal number of Krylov vectors to be used by the
                   solver for the little Dirac equation before a restart.

    nmx            Maximal total number of Krylov vectors generated by
                   the solver for the little Dirac equation.

    res            Required relative residue when solving the little
                   Dirac equation.

  The return value is a structure that contains the above parameters.

dfl_pro_parms_t dfl_pro_parms(void)
  Returns the parameters currently set for the deflation projectors in
  the deflated solver program dfl_sap_gcr().

dfl_gen_parms_t set_dfl_gen_parms(double kappa,double mu,
                                  int qhat,double su3csw,double u1csw,
                                  double cF,double cF_prime,
                                  double th1,double th2,double th3,
                                  int ninv,int nmr,int ncy)
  Sets the parameters of the inverse iteration procedure that generates
  the deflation subspace. The parameters are

    kappa          Hopping parameter of the Dirac operator.

    mu             Twisted mass parameter.

    qhat           Electric charge.

    su3csw,u1csw   Coefficients of the SU(3) and U(1) Sheikholeslami-Wohlert
                   term respectively.

    cF,cF_prime    Fermion action improvement coefficients at time 0
                   and T, respectively.

    th1,th2,th3    Angles specifying the phase-periodic boundary
                   conditions for the quark fields in direction 1,2,3.

    ninv           Total number of inverse iteration steps (ninv>=4).

    nmr            Number of block minimal residual iterations to be
                   used when the SAP smoother is applied.

    ncy            Number of SAP cycles per inverse iteration.

  The return value is a structure that contains the above parameters and
  the bare mass m0 that corresponds to the hopping parameter kappa.

dfl_gen_parms_t dfl_gen_parms(void)
  Returns the parameters currently set for the generation of the deflation
  subspace plus the corresponding bare mass m0.

dfl_upd_parms_t set_dfl_upd_parms(double dtau,int nsm)
  Sets the parameters of the deflation subspace update scheme. The
  parameters are

    dtau           Molecular-dynamics time separation between
                   updates of the deflation subspace.

    nsm            Number of deflated smoothing interations to be
                   applied when the subspace is updated.

  The return value is a structure that contains the above parameters.

dfl_upd_parms_t dfl_upd_parms(void)
  Returns the parameters currently set for the deflation subspace
  update scheme.

void print_dfl_parms(int ipr)
  Prints the parameters of the deflation subspace, the projectors, the
  subspace generation algorithm and the update scheme to stdout on MPI
  process 0. The update scheme is omitted if ipr=0.

void write_dfl_parms(FILE *fdat)
  Writes the parameters of the deflation subspace, the projectors, the
  subspace generation algorithm and the update scheme to the file fdat
  on MPI process 0.

void check_dfl_parms(FILE *fdat)
  Compares the parameters of the deflation subspace, the projectors the
  subspace generation algorithm and the update scheme with the values
  stored on the file fdat on MPI process 0, assuming the latter were
  written to the file by the program write_dfl_parms() (mismatches of
  maximal solver iteration numbers are not considered to be an error).

void read_dfl_parms(void)
  On process 0, this program reads the following sections from the stdin,
  as explained in more details in doc/parms.pdf

    [Deflation subspace]
    bs        <int> <int> <int> <int>
    Ns        <int>

    [Deflation subspace generation]
    kappa     <double>
    qhat      <int>
    mu        <double>
    su3csw    <double>
    u1csw     <double>
    cF        <double>
    cF'       <double>
    theta     <double> <double> <double>
    ninv      <int>
    nmr       <int>
    ncy       <int>

    [Deflation projection]
    nkv       <int>
    nmx       <int>
    res       <double>

    [Deflation update scheme]
    dtau      <double>
    nsm       <int>

  The first three sections are read independently of the value of 'update',
  while the fourth section is read only if update!=0.
  Depending on the the active gauge group and on the boundary conditions,
  some lines are not read and can be omitted in the input file. After
  reading the stdin, the deflation parameters are set with 'set_dfl_parms',
  'set_dfl_pro_parms', 'set_dfl_gen_parms', 'set_dfl_upd_parms'.

void set_flags(event_t event)
  Reports an event to the data base, which changed the global gauge
  gauge or SW fields.

void set_grid_flags(blk_grid_t grid,event_t event)
  Reports an event to the data base, which changed the gauge or SW
  fields on the specified block grid.

int query_flags(query_t query)
  Queries the data base on the status of the global gauge or SW
  fields. The program returns 1 or 0 depending on whether the answer
  to the specified query is "yes" or "no". If the query is unknown to
  the data base, the program returns -1.

int query_grid_flags(blk_grid_t grid,query_t query)
  Queries the data base on the status of the gauge or SW fields on
  the specified block grid. The program returns 1 or 0 depending on
  whether the answer to the specified query is "yes" or "no". If the
  query is unknown to the data base, the program returns -1.

void print_flags(void)
  Prints the current values of all flags related to the global gauge
  and SW fields to stdout from process 0.

void print_grid_flags(blk_grid_t grid)
  Prints the current values of all flags related to the gauge and SW
  fields on the specified block grid to stdout from process 0.

force_parms_t set_force_parms(int ifr,force_t force,int ipf,int im0,
                              int *irat,int *imu,int *isp,int *ncr)
  Sets the parameters in the force parameter set number ifr and returns
  a structure containing them (see the notes).

force_parms_t force_parms(int ifr)
  Returns a structure containing the force parameter set number ifr
  (see the notes).

void read_force_parms(int ifr)
  On process 0, this program scans stdin for a line starting with the
  string "[Force <int>]" (after any number of blanks), where <int> is
  the integer value passed by the argument. An error occurs if no such
  line or more than one is found. The lines

    force   <force_t>
    ipf     <int>
    im0     <int>
    irat    <int> <int> <int>
    imu     <int> [<int>]
    isp     <int> [<int>]
    ncr     <int> [<int>]

  are then read using read_line() [utils/mutils.c]. Depending on the
  value of "force", some lines are not read and can be omitted in the
  input file. The number of integer items on the lines with tag "imu"
  and "isp" and "ncr" depends on the force too. The data are then added
  to the data base by calling set_force_parms(ifr,...).

void read_force_parms2(int ifr)
  Same as read_force_parms() except that only the lines

    force   <force_t>
    isp     <int> [<int>]
    ncr     <int> [<int>]

  are read from stdin. All other force parameters are inferred from
  the parameters of the action no ifr so that the force is the one
  deriving from that action. An error occurs if the parameters of the
  action no ifr have not previously been added to the data base or
  if the force and action types do not match.

void print_force_parms(void)
  Prints the parameters of the defined forces to stdout on MPI
  process 0.

void print_force_parms2(void)
  Prints the parameters of the defined forces to stdout on MPI
  process 0 in a short format corresponding to read_force_parms2().

void write_force_parms(FILE *fdat)
  Writes the parameters of the defined forces to the file fdat on
  MPI process 0.

void check_force_parms(FILE *fdat)
  Compares the parameters of the defined forces with those stored
  on the file fdat on MPI process 0, assuming the latter were written
  to the file by the program write_force_parms().

hmc_parms_t set_hmc_parms(int nact,int *iact,int npf,int nmu,
                          double *mu,int nlv,double tau,int facc)
  Sets some basic parameters of the HMC algorithm. The parameters are

    nact        Number of terms in the total action

    iact        Indices iact[i] of the action terms (i=0,..,nact-1)

    npf         Number of pseudo-fermion fields on which the action
                depends

    nmu         Number of twisted mass parameters on which the
                pseudo-fermion actions and forces depend

    mu          Twisted masses mu[i] (i=0,..,nmu-1)

    nlv         Number of levels of the molecular-dynamics integrator

    tau         Molecular-dynamics trajectory length

    facc        U(1) Fourier-acceleration flag (facc=0 inactive,
                facc!=0 active); ignored if U(1) gauge field is not active.

  The total action must include the gauge action, but pseudo-fermion
  actions are optional and the momentum action is treated separately.
  The program returns a structure that contains the parameters listed
  above.

hmc_parms_t hmc_parms(void)
  Returns a structure containing the current values of the parameters
  listed above.

void print_hmc_parms(void)
  Prints the lattice parameters to stdout on MPI process 0.

flds_parms_t set_flds_parms(int gauge,int nfl)
  Sets the parameters which specify which fields need to be activated. The
  parameters are

    gauge          Active gauge fields.
                   1 : SU(3)
                   2 : U(1)
                   3 : SU(3)xU(1)

    nfl            Number of quark parameter sets (both valence and sea).

  The return value is a structure that contains these parameters.

flds_parms_t flds_parms(void)
  Returns the current field parameters in a structure that contains
  the above parameters.

su3lat_parms_t set_su3lat_parms(double beta,double c0,
                                double cG,double cG_prime,int SFtype)
  Sets the parameters for the SU(3) gauge action. The parameters are

    beta           Inverse SU(3) bare coupling (beta=6/g0^2).

    c0             Coefficient of the plaquette loops in the SU(3) gauge
                   action (see doc/gauge_action.pdf).

    cG,            SU(3) gauge action improvement coefficients at time 0
    cG_prime       and T, respectively.

    SFtype         Type of SF boundary for Luescher-Weisz actions
                   (0: orbifold constuction - 1: Aoki-Frezzotti-Weisz)

su3lat_parms_t su3lat_parms(void)
  Returns the current SU(3) gauge action parameters in a structure that
  contains the above parameters.

u1lat_parms_t set_u1lat_parms(int type,double alpha,double invqel,
                              double lambda,double c0,
                              double cG,double cG_prime,int SFtype)
  Sets the parameters for the U(1) gauge action. The parameters are

    type           Type of U(1) gauge action (0: compact, 1: non-compact)

    alpha          Bare fine-structure constant (alpha=1/(4 pi e0^2)).

    invqel         Inverse of the elementary electric charge. Only matter
                   with an electric charge equal to an integer value of
                   1.0/invqel is allowed.

    lambda         Gauge-fixing parameter (relevant only for the noncompact
                   formulation).

    c0             Coefficient of the plaquette loops in the compact U(1)
                   gauge action (relevant only for the compact formulation).

    cG,            U(1) gauge action improvement coefficients at time 0
    cG_prime       and T, respectively (relevant only for the compact
                   formulation).

    SFtype         Type of SF boundary for Luescher-Weisz actions
                   (0: orbifold constuction - 1: Aoki-Frezzotti-Weisz)

u1lat_parms_t u1lat_parms(void)
  Returns the current U(1) gauge action parameters in a structure that
  contains the above parameters.

dirac_parms_t set_qlat_parms(int ifl,double kappa,
                             int qhat,double su3csw,
                             double u1csw,double cF,double cF_prime,
                             double th1,double th2,double th3)
  Sets the Dirac-operator parameters for the inl-th quark flavour. The
  parameters are

    ifl            Number of quark flavour to be set.

    kappa          Hopping parameter.

    qhat           Integer electric charge (in units of the elementary one).

    su3csw         Coefficients of the SU(3) Sheikholeslami-Wohlert term.

    u1csw          Coefficients of the U(1) Sheikholeslami-Wohlert term.

    cF,cF_prime    Fermion action improvement coefficients at time 0 and T,
                   respectively.

    th1,th2,th3    Angles specifying the phase-periodic boundary conditions
                   for the quark fields in direction 1,2,3.

  The return value is a structure that contains the above parameters, but
  kappa which is replaced by the bare quark mass m0.

dirac_parms_t qlat_parms(int ifl)
  Returns the Dirac-operator parameters for the ifl-th quark flavour.

void print_lat_parms(void)
  Prints the lattice parameters to stdout on MPI process 0.

void write_flds_bc_lat_parms(FILE *fdat)
  Writes the global lattices sizes, fields, boundary and lattice parameters
  to the file fdat on MPI process 0.

void check_flds_bc_lat_parms(FILE *fdat)
  Compares the global lattice sizes, fields, boundary and the lattice
  parameters with the values stored on the file fdat on MPI process 0,
  assuming the latter were written to the file by the program
  write_flds_bc_lat_parms().

bc_parms_t set_bc_parms(int type,int cstar,
                        double *phi3,double *phi3_prime,
                        double phi1,double phi1_prime)
  Sets the boundary conditions and the associated parameters of the
  action. The parameters are

    type           Chosen type of time boundary condition (0: open, 1: SF,
                   2: open-SF, 3: periodic).

    cstar          Number of spatial directions with C-periodic boundary
                   conditions.

    phi3[0],       First two angles that define the boundary values of
    phi3[1]        the SU(3) gauge field at time 0.

    phi3_prime[0], First two angles that define the boundary values of
    phi3_prime[1]  the SU(3) gauge field at time T.

    phi1           Angle that defines the boundary values of the U(1)
                   gauge field at time 0.

    phi1_prime     Angle that defines the boundary values of the U(1)
                   gauge field at time T.

  The return value is a structure that contains these parameters plus
  the third SU(3) angles. In this structure, the angles are stored in the
  form of arrays phi3[2][3], where phi3[0][] and phi3[1][] are the
  parameters at time 0 and T, respectively.

bc_parms_t bc_parms(void)
  Returns a structure that contains the boundary parameters.

void print_bc_parms(void)
  Prints the boundary parameters to stdout on MPI process 0.

int bc_type(void)
  Returns the type of the chosen boundary conditions (0: open, 1: SF,
  2: open-SF, 3: periodic).

void read_bc_parms(void)
  On process 0, this program reads the following section from the stdin,
  as explained in more details in doc/parms.pdf

    [Boundary conditions]
    type      <string|int>
    cstar     <int>
    su3phi    <double> <double>
    su3phi'   <double> <double>
    u1phi     <double>
    u1phi'    <double>

  This program assumes that 'set_flds_parms' has been already called.
  Depending on the the gauge group selected with 'set_flds_parms', some
  lines are not read and can be omitted in the input file. After reading
  the stdin, the boundary conditions parameters are set with 'set_bc_parms'.

void read_glat_parms(void)
  On process 0, this program reads a number of sections from the stdin
  with the parameters of the gauge actions, as explained in more details
  in doc/parms.pdf. This program assumes that 'set_flds_parms' and
  'set_bc_parms' have been already called. If the SU(3) gauge field is
  active, the program reads the following section

    [SU(3) action]
    beta         <double>
    c0           <double>
    SFtype       <string|int>
    cG           <double>
    cG'          <double>

  Depending on the boundary conditions, some lines are not read and can be
  omitted in the input file. After reading the stdin, the SU(3) action
  parameters are set with 'set_su3lat_parms'.

  If the U(1) gauge field is active, the program reads the following section

    [U(1) action]
    type         <string|int>
    alpha        <double>
    invqel       <double>
    c0           <double>
    SFtype       <string|int>
    cG           <double>
    cG'          <double>

  Depending on the boundary conditions, some lines are not read and can be
  omitted in the input file. After reading the stdin, the U(1) action
  parameters are set with 'set_u1lat_parms'.

void read_qlat_parms(void)
  On process 0, this program reads a number of sections from the stdin
  with the parameters of the quark actions, as explained in more details
  in doc/parms.pdf. This program assumes that 'set_flds_parms' and
  'set_bc_parms' have been already called.

  If nfl is the number of quark flavours set with the 'set_flds_parms'
  program, for each ifl=0,...,nfl-1, the program reads the following
  sections

    [Flavour ifl]
    qhat         <int>
    kappa        <double>
    su3csw       <double>
    u1csw        <double>
    cF           <double>
    cF'          <double>
    theta        <double>

  Depending on the boundary conditions and the active gauge, some lines are
  not read and can be omitted in the input file. After reading the stdin,
  the quark flavour parameters are set with 'set_qlat_parms'.

mdint_parms_t set_mdint_parms(int ilv,integrator_t integrator,double lambda,
                              int nstep,int nfr,int *ifr)
  Sets the parameters of the molecular-dynamics integrator at level
  ilv and returns a structure containing them (see the notes).

mdint_parms_t mdint_parms(int ilv)
  Returns a structure containing the parameters of the integrator at
  level ilv (see the notes).

void read_mdint_parms(int ilv)
  On process 0, this program scans stdin for a line starting with the
  string "[Level <int>]" (after any number of blanks), where <int> is
  the integer value passed by the argument. An error occurs if no such
  line or more than one is found. The lines

    integrator   <integrator_t>
    lambda       <double>
    nstep        <int>
    forces       <int> [<int>]

  are then read using read_line() [utils/mutils.c]. The line tagged
  "lambda" is required only when the specified integrator is the 2nd
  order OMF integrator. The line tagged "forces" must contain the
  indices of the forces (separated by white space) that are to be
  integrated at this level. On exit, the data are entered in the data
  base by calling set_mdint_parms(ilv,...).

void print_mdint_parms(void)
  Prints the parameters of the defined integrator levels to stdout
  on MPI process 0.

void write_mdint_parms(FILE *fdat)
  Writes the parameters of the defined integrator levels to the file
  fdat on MPI process 0.

void check_mdint_parms(FILE *fdat)
  Compares the parameters of the defined integrator levels with those
  stored on the file fdat on MPI process 0, assuming the latter were
  written to the file by the program write_mdint_parms().

rat_parms_t set_rat_parms(int irp,int num,int den,int degree,double *range,
                          double A,double *nu,double *mu,double delta,
                          double *x)
  Sets the parameters in the rational function parameter set number
  irp and returns a structure containing them (see the notes).

rat_parms_t rat_parms(int irp)
  Returns a structure containing the rational function parameter set
  number irp (see the notes).

void read_rat_parms(int irp)
  On process 0, this program scans stdin for a line starting with the
  string "[Rational <int>]" (after any number of blanks), where <int> is
  the integer value passed by the argument. An error occurs if no such
  line or more than one is found. If a rational approximation for
  (x+mu^2)^(-1/2) is needed, the input file section must look like

    power   -1 2
    degree  <int>
    range   <double> <double>
    mu      <double>

  In this case the rational approzimation is calculated by the function
  zolotarev() (see the ratfcts module directory), called by the
  set_rat_parms function. If a rational approximation for a different
  power is needed, the input file section must look like

    power   <int> <int>
    degree  <int>
    range   <double> <double>
    mu      <double>
    A       <double>
    delta   <double>
    nu[0]    <double>
    ...
    nu[M]    <double>
    mu[0]    <double>
    ...
    mu[M]    <double>
    x[0]    <double>
    ...
    x[N]    <double>

  where M=degree-1 and N=2*degree+1. A is the normalization of the
  rational approximation, nu[i] are the square-roots of the (-1)*zeroes of
  the numerator, mu[i] are the square-roots of the (-1)*zeroes of the
  denominator, x[i] are the extrema of the error function, and delta is the
  signed relative error calculated at the leftmost extremum.

void print_rat_parms(void)
  Prints the defined rational function parameter sets to stdout on MPI
  process 0.

void write_rat_parms(FILE *fdat)
  Writes the defined rational function parameter sets to the file fdat
  on MPI process 0.

void check_rat_parms(FILE *fdat)
  Compares the defined rational function parameter sets with those
  on the file fdat on MPI process 0, assuming the latter were written
  to the file by the program write_rat_parms().

rw_parms_t set_rw_parms(int irw,rwfact_t rwfact,int ifl,int nsrc,
                        int irp1,int irp2,int nfct,double *mu,int *np,
                        int *isp)
  Sets the parameters in the reweighting factor parameter set number
  irw and returns a structure containing them (see the notes).

rw_parms_t rw_parms(int irw)
  Returns a structure containing the reweighting factor parameter set
  number irw (see the notes).

void read_rw_parms(int irw)
  On process 0, this program scans stdin for a line starting with the
  string "[Reweighting factor <int>]" (after any number of blanks), where
  <int> is the integer value passed through the argument. An error occurs
  if no such line or more than one is found. The lines

    rwfact   <rwfact_t>
    im0      <int>
    nsrc     <int>
    irp      <int> [<int>]
    mu       <double> [<double>]
    np       <int> [<int>]
    isp      <int> [<int>]

  are then read using read_line() [utils/mutils.c] and the data are
  added to the data base by calling set_rw_parms(irw,...). Depending
  on the value of "rwfact", some lines are not read and can be omitted
  in the input file. The number of items on the lines with tag "irp", "mu",
  "np" and "isp" depends on the reweighting factor too (see the notes).

void print_rw_parms(void)
  Prints the defined reweighting factor parameter sets to stdout on
  MPI process 0.

void write_rw_parms(FILE *fdat)
  Writes the defined reweighting factor parameter sets to the file fdat
  on MPI process 0.

void check_rw_parms(FILE *fdat)
  Compares the defined reweighting factor parameter sets with those
  on the file fdat on MPI process 0, assuming the latter were written
  to the file by the program write_rw_parms().

sap_parms_t set_sap_parms(int *bs,int isolv,int nmr,int ncy)
  Sets the parameters of the SAP preconditioner. The parameters are

    bs[4]         Sizes of the blocks in SAP_BLOCKS block grid.

    isolv         Block solver to be used (0: plain MinRes,
                  1: eo-preconditioned MinRes).

    nmr           Number of block solver iterations.

    ncy           Number of SAP cycles to be applied.

  The return value is a structure that contains the parameters of the
  SAP preconditioners. The block sizes bs[4] can only be set once, but
  the values of the other parameters may be changed by calling the
  program again.

sap_parms_t sap_parms(void)
  Returns the parameters currently set for the SAP preconditioner.

void print_sap_parms(int ipr)
  Prints the SAP parameters to stdout on MPI process 0. Depending
  on whether ipr!=0 or 0, the full information is printed or only
  the block size.

void write_sap_parms(FILE *fdat)
  Writes the SAP parameters to the file fdat on MPI process 0.

void check_sap_parms(FILE *fdat)
  Compares the SAP parameters with the values stored on the file fdat
  on MPI process 0, assuming the latter were written to the file by
  the program write_sap_parms().

void read_sap_parms(void)
  On process 0, this program reads the following section from the stdin,
  as explained in more details in doc/parms.pdf

    [SAP]
    bs        <int> <int> <int> <int>

  After reading the stdin, the SAP parameters are set with 'set_sap_parms'.

sf_parms_t set_sf_parms(double *phi,double *phi_prime)
  Sets the parameters of the boundary fields in the Schroedinger
  functional. The parameters are

    phi           Angles phi[0],phi[1] at time 0.

    phi_prime     Angles phi[0],phi[1] at time T.

  See the notes for further explanations. This program may only be
  called once.

sf_parms_t sf_parms(void)
  Returns the parameters of the boundary fields currently set.

void print_sf_parms(void)
  Prints the parameters of the boundary fields to stdout on MPI
  process 0.

void write_sf_parms(FILE *fdat)
  Writes the parameters of the boundary fields to the file fdat on
  MPI process 0.

void check_sf_parms(FILE *fdat)
  Compares the parameters of the boundary fields with the values
  stored on the file fdat on MPI process 0, assuming the latter were
  written to the file by the program write_sf_parms().

int sf_flg(void)
  Returns 1 if the Schroedinger-functional boundary values have been
  initialized and 0 otherwise.

solver_parms_t set_solver_parms(int isp,solver_t solver,
                                int nkv,int isolv,int nmr,int ncy,
                                int nmx,double res)
  Sets the parameters in the solver parameter set number isp and returns
  a structure containing them (see the notes).

solver_parms_t solver_parms(int isp)
  Returns a structure containing the solver parameter set number
  isp (see the notes).

void read_solver_parms(int isp)
  On process 0, this program scans stdin for a line starting with the
  string "[Solver <int>]" (after any number of blanks), where <int> is
  the integer value passed by the argument. An error occurs if no such
  line or more than one is found. The lines

    solver  <solver_t>
    nkv     <int>
    isolv   <int>
    nmr     <int>
    ncy     <int>
    nmx     <int>
    res     <double>

  are then read one by one using read_line() [utils/mutils.c]. The
  lines with tags nkv,..,ncy may be absent in the case of the CGNE
  and MSCG solvers (see the notes). The data are then added to the
  data base by calling set_solver_parms(isp,...).

void print_solver_parms(int *isap,int *idfl)
  Prints the parameters of the defined solvers to stdout on MPI
  process 0. On exit the flag isap is 1 or 0 depending on whether
  one of the solvers makes use of the Schwarz Alternating Procedure
  (SAP) or not. Similarly, the flag idfl is set 1 or 0 depending on
  whether deflation is used or not. On MPI processes other than 0,
  the program does nothing and sets isap and idfl to zero.

void write_solver_parms(FILE *fdat)
  Writes the parameters of the defined solvers to the file fdat on
  MPI process 0.

void check_solver_parms(FILE *fdat)
  Compares the parameters of the defined solvers with those stored
  on the file fdat on MPI process 0, assuming the latter were written
  to the file by the program write_solver_parms(). Mismatches of the
  maximal solver iteration number are not considered to be an error.

dft_parms_t *dft_parms(int id)
  Returns the DFT parameter set with the given id. If there is no
  such parameter set, the program returns NULL.

int set_dft4d_parms(int *idp,int *nx,int csize)
  Initializes a new instance of a DFT4D parameter set for the Fourier
  transformation of fields in four dimensions (see the notes). The
  program returns the id of the parameter set in the data base.

dft4d_parms_t *dft4d_parms(int id)
  Returns the DFT4D parameter set with the given id. If there is no
  such parameter set, the program returns NULL.
