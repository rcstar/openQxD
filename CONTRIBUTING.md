# Contributing
## Introduction
Thanks for considering contributing to RC*
Contributions are welcome.
You can contribute by improving the documentation, submit bug reports,
provide feature requests, or sending us feedback on how the code runs.
## Ground Rules
We aim at having this code as tested by the community as possible.
## Repositories
* Main: <https://gitlab.com/rcstar/openQxD>
## How to report a bug
Issues and or feature enhancements can be communicated on GitLab
<https://gitlab.com/rcstar/openQxD/issues>
## Code contributions
* Get in touch with the authors at agostino.patella@physik.hu-berlin.de
